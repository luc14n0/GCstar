# Initial spec file created by autospec ver. 0.8 with rpm 3 compatibility

Summary: GCstar, Collection manager
Name: gcstar
Version: 1.8.0
Release: 1
Group: Applications/Databases
License: GPLv2
Source: gcstar-%{version}.tar.gz
Requires: perl-Gtk3
BuildRoot: %{_tmppath}/%{name}-root
BuildArch: noarch
URL: https://www.gitlab.com/Kerenoc/GCstar/tree/Gtk3

%description
GCstar - Application for managing your personal collections (books, films, games...).
Detailed information on items can be automatically retrieved
from the internet. New type of collections can be added. Lending to other people can be tracked.

%description -l fr
GCstar - Application permettant de gérer des collections personnelles (films, livres, jeux...).
Des recherches internet permettent de compléter les descriptions des éléments. Le prêt d'éléments est géré.
De nouveaux types de collections peuvent être ajoutés par les utilisateurs.

%prep
%setup -q -n %{name}

%install
./install --prefix=${RPM_BUILD_ROOT}/usr/ --verbose --text --noclean

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(-,root,root)
/usr/lib/gcstar/
/usr/share/gcstar/
/usr/share/applications/gcstar.desktop
/usr/share/icons/hicolor/*/apps/gcstar.png
/usr/share/icons/hicolor/*/mimetypes/application-x-gcstar.png
/usr/share/man/man1/gcstar.1.gz
/usr/share/mime/application/x-gcstar.xml
/usr/share/mime/packages/gcstar.xml
/usr/share/pixmaps/gcstar.png
%attr(0755,root,root) /usr/bin/gcstar

%changelog
* Mon Apr 03 2023 Kerenoc
  - Prepare 1.8.0 version
* Wed Dec 09 2019 Kerenoc
  - Fix build process
* Tue Feb 08 2005 Tian <tian@c-sait.net>
  - Initial spec file created by autospec ver. 0.8 with rpm 3 compatibility

#! sh
#
# Manual installation Linux Mint
#
git clone http://gitlab.com/GCstar/GCstar.git

cd GCstar/gcstar/bin

sudo apt install libgtk3-perl
sudo cpan install Gtk3::SimpleList

# minimum configuration
perl gcstar 

sudo apt install libgd-perl
sudo apt install libgd-graph-perl
sudo apt install libmp3-info-perl
sudo apt install libmp3-tag-perl
sudo apt install libimage-exiftool-perl
sudo apt install libdate-calc-perl
sudo apt install libdatetime-format-strptime-perl
sudo apt install libnet-freedb-perl
sudo apt install libogg-vorbis-header-pureperl-perl

# full configuration
perl gcstar

Name:		gcstar
Version:    1.8.0
Release:	1%{?dist}
Summary:	Personal collections manager

Group:		Applications/Internet
License:	GPLv2+
URL:		https://gitlab.com/GCstar/GCstar
Source:         gcstar-%{version}.tar.gz 
BuildArch:	noarch

Patch0:		patch-install
Patch1:		patch-gcstar



Requires:	perl-interpreter, perl-Gtk3, perl-HTML-Parser, perl-libwww-perl
Requires:	perl-LWP-Protocol-https, perl-Date-Calc, perl-GDGraph
Requires:	perl-DateTime-Format-Strptime, perl-XML-Parser
Requires:	perl-XML-Simple, perl-GDGraph, perl-Archive-Tar
Requires:	perl-Archive-Zip, perl-GDTextUtil
Requires:	perl-MP3-Info, perl-Time-Piece, perl-Image-ExifTool, perl-JSON
Requires:       perl-Gtk3-SimpleList, webp-pixbuf-loader

%description
GCstar is an application for managing your personal collections.
Detailed information on each item can be automatically retrieved
from the internet and you can store additional data, depending on
the collection type. You can also lent items (date, persons, status). You
may also search and filter your collection by criteria.

%prep

%setup -q -n gcstar
%patch0 -p0
%patch1 -p0

%build

%install

install -d %{buildroot}%{_prefix}/share/pixmaps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/16x16/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/24x24/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/32x32/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/36x36/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/48x48/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/64x64/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/72x72/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/96x96/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/128x128/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/128x128/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/192x192/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/256x256/apps
install -d %{buildroot}%{_prefix}/share/icons/hicolor/scalable/apps
install -d %{buildroot}%{_prefix}/share/mime/packages

./install --prefix=%{buildroot}%{_prefix} --verbose --text --noclean

%files

%doc

%{_bindir}/%{name}
%{_datadir}/%{name}/LICENSE

%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/fonts
%dir %{_datadir}/%{name}/genres
%dir %{_datadir}/%{name}/helpers
%dir %{_datadir}/%{name}/html_models
%dir %{_datadir}/%{name}/html_models/GCboardgames
%dir %{_datadir}/%{name}/html_models/GCbooks
%dir %{_datadir}/%{name}/html_models/GCcoins
%dir %{_datadir}/%{name}/html_models/GCfilms
%dir %{_datadir}/%{name}/html_models/GCgadgets
%dir %{_datadir}/%{name}/html_models/GCgames
%dir %{_datadir}/%{name}/html_models/GCminicars
%dir %{_datadir}/%{name}/html_models/GCmusics
%dir %{_datadir}/%{name}/html_models/GCstar
%dir %{_datadir}/%{name}/html_models/GCmusiccourses
%dir %{_datadir}/%{name}/icons
%dir %{_datadir}/%{name}/lib
%dir %{_datadir}/%{name}/lib/GCBackend
%dir %{_datadir}/%{name}/lib/GCExport
%dir %{_datadir}/%{name}/lib/GCExtract
%dir %{_datadir}/%{name}/lib/GCGraphicComponents
%dir %{_datadir}/%{name}/lib/GCImport
%dir %{_datadir}/%{name}/lib/GCItemsLists
%dir %{_datadir}/%{name}/lib/GCLang
%dir %{_datadir}/%{name}/lib/GCLang/AR
%dir %{_datadir}/%{name}/lib/GCLang/AR/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/AR/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/AR/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/BG
%dir %{_datadir}/%{name}/lib/GCLang/BG/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/BG/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/BG/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/CA
%dir %{_datadir}/%{name}/lib/GCLang/CA/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/CA/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/CA/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/CS
%dir %{_datadir}/%{name}/lib/GCLang/CS/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/CS/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/CS/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/DE
%dir %{_datadir}/%{name}/lib/GCLang/DE/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/DE/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/DE/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/EL
%dir %{_datadir}/%{name}/lib/GCLang/EL/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/EL/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/EL/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/EN
%dir %{_datadir}/%{name}/lib/GCLang/EN/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/EN/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/EN/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/ES
%dir %{_datadir}/%{name}/lib/GCLang/ES/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/ES/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/ES/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/FR
%dir %{_datadir}/%{name}/lib/GCLang/FR/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/FR/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/FR/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/GL
%dir %{_datadir}/%{name}/lib/GCLang/GL/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/GL/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/GL/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/HU
%dir %{_datadir}/%{name}/lib/GCLang/HU/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/HU/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/HU/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/ID
%dir %{_datadir}/%{name}/lib/GCLang/ID/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/ID/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/ID/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/IT
%dir %{_datadir}/%{name}/lib/GCLang/IT/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/IT/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/IT/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/NL
%dir %{_datadir}/%{name}/lib/GCLang/NL/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/NL/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/NL/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/PL
%dir %{_datadir}/%{name}/lib/GCLang/PL/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/PL/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/PL/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/PT
%dir %{_datadir}/%{name}/lib/GCLang/PT/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/PT/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/PT/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/RO
%dir %{_datadir}/%{name}/lib/GCLang/RO/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/RO/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/RO/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/RU
%dir %{_datadir}/%{name}/lib/GCLang/RU/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/RU/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/RU/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/SR
%dir %{_datadir}/%{name}/lib/GCLang/SR/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/SR/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/SR/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/SV
%dir %{_datadir}/%{name}/lib/GCLang/SV/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/SV/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/SV/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/TR
%dir %{_datadir}/%{name}/lib/GCLang/TR/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/TR/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/TR/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/UK
%dir %{_datadir}/%{name}/lib/GCLang/UK/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/UK/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/UK/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/ZH
%dir %{_datadir}/%{name}/lib/GCLang/ZH/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/ZH/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/ZH/GCImport
%dir %{_datadir}/%{name}/lib/GCLang/ZH_CN
%dir %{_datadir}/%{name}/lib/GCLang/ZH_CN/GCModels
%dir %{_datadir}/%{name}/lib/GCLang/ZH_CN/GCExport
%dir %{_datadir}/%{name}/lib/GCLang/ZH_CN/GCImport
%dir %{_datadir}/%{name}/lib/GCModels
%dir %{_datadir}/%{name}/lib/GCPlugins
%dir %{_datadir}/%{name}/lib/GCPlugins/GCTVepisodes
%dir %{_datadir}/%{name}/lib/GCPlugins/GCTVseries
%dir %{_datadir}/%{name}/lib/GCPlugins/GCboardgames
%dir %{_datadir}/%{name}/lib/GCPlugins/GCbooks
%dir %{_datadir}/%{name}/lib/GCPlugins/GCbuildingtoys
%dir %{_datadir}/%{name}/lib/GCPlugins/GCcoins
%dir %{_datadir}/%{name}/lib/GCPlugins/GCcomics
%dir %{_datadir}/%{name}/lib/GCPlugins/GCfilms
%dir %{_datadir}/%{name}/lib/GCPlugins/GCgadgets
%dir %{_datadir}/%{name}/lib/GCPlugins/GCgames
%dir %{_datadir}/%{name}/lib/GCPlugins/GCmusics
%dir %{_datadir}/%{name}/lib/GCPlugins/GCmusiccourses
%dir %{_datadir}/%{name}/lib/GCPlugins/GCstar
%dir %{_datadir}/%{name}/list_bg
%dir %{_datadir}/%{name}/list_bg/Box
%dir %{_datadir}/%{name}/list_bg/Brick_and_Glass
%dir %{_datadir}/%{name}/list_bg/Dark_Glass
%dir %{_datadir}/%{name}/list_bg/Glass
%dir %{_datadir}/%{name}/list_bg/Green_Glass
%dir %{_datadir}/%{name}/list_bg/Luxury_Green_Glass
%dir %{_datadir}/%{name}/list_bg/Luxury_Green_Wood
%dir %{_datadir}/%{name}/list_bg/Luxury_Grey_Glass
%dir %{_datadir}/%{name}/list_bg/Luxury_Grey_Wood
%dir %{_datadir}/%{name}/list_bg/Luxury_Purple_Glass
%dir %{_datadir}/%{name}/list_bg/Luxury_Purple_Wood
%dir %{_datadir}/%{name}/list_bg/Luxury_Red_Glass
%dir %{_datadir}/%{name}/list_bg/Luxury_Red_Wood
%dir %{_datadir}/%{name}/list_bg/Marble
%dir %{_datadir}/%{name}/list_bg/Wood
%dir %{_datadir}/%{name}/list_bg/Wood2
%dir %{_datadir}/%{name}/list_bg/Wood_and_Glass
%dir %{_datadir}/%{name}/logos
%dir %{_datadir}/%{name}/overlays
%dir %{_datadir}/%{name}/panels

%dir %{_datadir}/%{name}/schemas
%dir %{_datadir}/%{name}/style
%dir %{_datadir}/%{name}/style/GCstar
%dir %{_datadir}/%{name}/style/GCstar/icons
%dir %{_datadir}/%{name}/style/GCstar/icons/about
%dir %{_datadir}/%{name}/style/GCstar/icons/add
%dir %{_datadir}/%{name}/style/GCstar/icons/cancel
%dir %{_datadir}/%{name}/style/GCstar/icons/clear
%dir %{_datadir}/%{name}/style/GCstar/icons/convert
%dir %{_datadir}/%{name}/style/GCstar/icons/delete
%dir %{_datadir}/%{name}/style/GCstar/icons/directory
%dir %{_datadir}/%{name}/style/GCstar/icons/dnd
%dir %{_datadir}/%{name}/style/GCstar/icons/error
%dir %{_datadir}/%{name}/style/GCstar/icons/execute
%dir %{_datadir}/%{name}/style/GCstar/icons/find
%dir %{_datadir}/%{name}/style/GCstar/icons/go-back
%dir %{_datadir}/%{name}/style/GCstar/icons/go-forward
%dir %{_datadir}/%{name}/style/GCstar/icons/go-down
%dir %{_datadir}/%{name}/style/GCstar/icons/go-up
%dir %{_datadir}/%{name}/style/GCstar/icons/help
%dir %{_datadir}/%{name}/style/GCstar/icons/home
%dir %{_datadir}/%{name}/style/GCstar/icons/jump-to
%dir %{_datadir}/%{name}/style/GCstar/icons/media-next
%dir %{_datadir}/%{name}/style/GCstar/icons/media-play
%dir %{_datadir}/%{name}/style/GCstar/icons/network
%dir %{_datadir}/%{name}/style/GCstar/icons/new
%dir %{_datadir}/%{name}/style/GCstar/icons/ok
%dir %{_datadir}/%{name}/style/GCstar/icons/open
%dir %{_datadir}/%{name}/style/GCstar/icons/preferences
%dir %{_datadir}/%{name}/style/GCstar/icons/properties
%dir %{_datadir}/%{name}/style/GCstar/icons/quit
%dir %{_datadir}/%{name}/style/GCstar/icons/refresh
%dir %{_datadir}/%{name}/style/GCstar/icons/remove
%dir %{_datadir}/%{name}/style/GCstar/icons/revert-to-saved
%dir %{_datadir}/%{name}/style/GCstar/icons/save-as
%dir %{_datadir}/%{name}/style/GCstar/icons/save
%dir %{_datadir}/%{name}/style/GCstar/icons/select-color
%dir %{_datadir}/%{name}/style/Gtk
%dir %{_datadir}/%{name}/style/kde

%dir %{_datadir}/%{name}/xml_models
%dir %{_datadir}/%{name}/xml_models/GCfilms

%{_datadir}/%{name}/lib/GCLang/*/GCExport/*.pm
%{_datadir}/%{name}/lib/GCLang/*/GCImport/*.pm
%{_datadir}/%{name}/lib/GCLang/*/GCModels/*.pm
%{_datadir}/%{name}/lib/GCLang/*/GCstar.pm

%{_datadir}/%{name}/list_bg/*/style

%{_datadir}/%{name}/xml_models/GCfilms/Ant_Movie_Catalog
%{_datadir}/%{name}/xml_models/GCfilms/DVDProfiler

%{_datadir}/%{name}/style/*/gtkrc
%{_datadir}/%{name}/style/Gtk/gtk-3.0/
%{_datadir}/%{name}/style/kde/gtkrcold

%{_datadir}/%{name}/lib/*.pm

%{_datadir}/%{name}/lib/*/*.pm
%{_datadir}/%{name}/lib/GCPlugins/*/*.pm

%{_datadir}/%{name}/lib/list_modified_files.txt

%{_datadir}/%{name}/lib/GCPlugins/*/*.pm_archive
%{_datadir}/%{name}/lib/GCLang/IT/README.txt
%{_datadir}/%{name}/lib/GCLang/README

%{_datadir}/%{name}/lib/GCModels/*.gcm

%{_mandir}/man1/%{name}.1.gz

%{_datadir}/%{name}/fonts/ChangeLog
%{_datadir}/%{name}/fonts/LICENSE
%{_datadir}/%{name}/fonts/LiberationSans-Regular.ttf
%{_datadir}/%{name}/fonts/README
%{_datadir}/%{name}/fonts/AUTHORS

%{_datadir}/%{name}/genres/*.genres

%{_datadir}/%{name}/helpers/xdg-open

%{_datadir}/%{name}/icons/*.ico
%{_datadir}/%{name}/icons/*.png
%{_datadir}/%{name}/logos/*.png
%{_datadir}/%{name}/overlays/*.png

%{_datadir}/%{name}/*/*/*.png

%{_datadir}/%{name}/panels/Classic
%{_datadir}/%{name}/panels/Dark
%{_datadir}/%{name}/panels/WebSite

%{_datadir}/%{name}/schemas/gcm*

%{_datadir}/%{name}/html_models/GCboardgames/piwi
%{_datadir}/%{name}/html_models/GCfilms/Flat
%{_datadir}/%{name}/html_models/GCbooks/FloFred
%{_datadir}/%{name}/html_models/GCbooks/NellistosDark
%{_datadir}/%{name}/html_models/GCbooks/NellistosLight
%{_datadir}/%{name}/html_models/GCcoins/Mancoliste
%{_datadir}/%{name}/html_models/GCgadgets/*
%{_datadir}/%{name}/html_models/GCgames/*
%{_datadir}/%{name}/html_models/GCmusiccourses/Flat
%{_datadir}/%{name}/html_models/GCmusiccourses/Tabs
%{_datadir}/%{name}/html_models/GCmusiccourses/Tian
%{_datadir}/%{name}/html_models/GCmusiccourses/Tian-Mario
%{_datadir}/%{name}/html_models/GCmusiccourses/Tian-Mario-Kim
%{_datadir}/%{name}/html_models/GCmusiccourses/float
%{_datadir}/%{name}/html_models/GCmusiccourses/rootII_design

%{_datadir}/%{name}/html_models/*/Shelf
%{_datadir}/%{name}/html_models/*/Simple

%{_datadir}/%{name}/html_models/GCcoins/Simple_bis
%{_datadir}/%{name}/html_models/GCfilms/Tabs
%{_datadir}/%{name}/html_models/GCfilms/Tian
%{_datadir}/%{name}/html_models/GCfilms/Tian-Mario
%{_datadir}/%{name}/html_models/GCfilms/Tian-Mario-Kim
%{_datadir}/%{name}/html_models/GCfilms/float
%{_datadir}/%{name}/html_models/GCfilms/rootII_design
%{_datadir}/%{name}/html_models/GCgames/Flat
%{_datadir}/%{name}/html_models/GCgames/Tabs

%{_datadir}/%{name}/html_models/GCminicars/Tian-Jim

%dir %{_datadir}/applications

%{_datadir}/applications/mimeinfo.cache


%{_datadir}/applications/%{name}.desktop

%{_datadir}/%{name}/style/GCstar/icons/*/*.png

%{_datadir}/pixmaps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/mimetypes/application-x-%{name}.png

%{_datadir}/%{name}/icons/gcstar_scalable.svg
%{_datadir}/%{name}/logos/Peri_main_logo.svg
%{_datadir}/%{name}/logos/periscope_main_logo.svg
#%{_datadir}/icons/hicolor/scalable/apps/%{name}.svg
%{_datadir}/mime/packages/%{name}.xml
%{_datadir}/mime/*


%post
gtk-update-icon-cache %{_datadir}/icons/hicolor
update-desktop-database
update-mime-database %{_datadir}/mime

%postun
gtk-update-icon-cache %{_datadir}/icons/hicolor
update-desktop-database
update-mime-database %{_datadir}/mime


%changelog
* Mon Apr 3 2023 Kerenoc
- prepare 1.8.0

* Tue Mar 28 2023 Kerenoc
- remove Provides fields
- remove duplicatesv

* Mon Dec 9 2019 Kerenoc
- Adaptation for version 1.7.3 Gtk3

* Thu Nov 8 2018 Nuno Dias <Nuno.Dias@gmail.com> - 1.7.2-1.ndias
- Release Version 1.7.2 from gitlab

* Wed Jul 18 2018 Nuno Dias <Nuno.Dias@gmail.com> - 1.7.2-gitlab20180718.1.ndias
- Version 1.7.2 from git

# GCstar installation

GCstar is based on the Perl language and the GTK3 library.

## Warning

Before installing a new version of GCstar, don't forget to make some backups of all your collections!

## Linux

On some Linux distributions, GCstar is available as a package and can be installed using the application manager or with a command line. GCstar can be installed from the source code using the following instructions.

### Debian based distributions

#### Installation using a deb package ####

Visit the [GCstar package registry page](https://gitlab.com/GCstar/GCstar/-/packages/10814845) and download the latest .deb file then execute

````
sudo apt install ./gcstar*.deb
````
On Bullseye, the *webp-pixbuf-loader* package is not included but is available in the Bullseye-Backport repository. Before installing GCstar, execute

````
sudo cat >> /etc/apt/sources.list <<EOFEOFEOF
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
EOFEOFEOF
apt update
````

#### Manual dependencies installation with *apt* 

Execute the following commands (or similar commands according to the Linux distribution) to install Perl and the mandatory libraries for a minimalistic GCstar (no plugins)

````
sudo apt-get install perl
sudo apt-get install libgtk3-perl
sudo apt-get install libgtk3-simplelist-perl
sudo apt-get install libxml-simple-perl
````

To use plugins (to download information from the web), install additional libraries

````
sudo apt-get install libjson-perl
sudo apt-get install libdatetime-format-strptime-perl
sudo apt-get install liblocale-codes-perl
````

#### Download and prepare the installation

In the target directory for GCstar, execute the following commands 

````
wget -O - https://gitlab.com/GCstar/GCstar/-/archive/main/GCstar-main.tar.gz | tar xzf -
mv GCstar-main GCstar
````

You may test GCstar (without the statistics function) with

````
cd GCstar/gcstar/bin
perl gcstar
cd ..
````

#### Manual installation by locally building a deb package ####

In the *GCstar* directory

````
cp -r gcstar/packages/debian .
dpkg-buildpackage -rfakeroot -b -uc -us --no-sign
cd ..
ls gcstar_*.deb | sort -V | tail -n1 | xargs sudo apt-get --fix-broken install
````

To run GCstar, simply type

````
gcstar
````

If GCstar was already installed, copy the **gcstar** script in the appropriate binaries directory (probably **/usr/bin**, check with the "which gcstar" command) and copy the **lib** directory in the libraries directory (probably **/usr/lib/gcstar** or **/usr/share/gcstar/lib**).


## Windows 

For the latest versions, Windows installers are available from the [Package Registry](https://gitlab.com/GCstar/GCstar/-/packages/10814845). Installers are also available for older versions : [1.7.2](https://gitlab.com/GCstar/GCstar/-/tags/v1.7.2) and [1.7.3](https://gitlab.com/GCstar/GCstar/-/tags/v1.7.3).

After being developped on Windows using ActivePerl then Strawberry Perl, as these environment don't provide a reliable support of Gtk, GCstar is now based on MSYS2/mingw64. The installation of all the dependencies requires some tweaking that was implemented in the [build-GCstar-mingw64.sh](https://gitlab.com/GCstar/GCstar/-/blob/main/tools/build-GCstar-mingw64.sh) script.

From a Mingw64 shell, run the following commands (taken from the [Gitlab's Continuous Integration recipe](https://gitlab.com/GCstar/GCstar/-/blob/main/.gitlab-ci.yml#L131) in the GCstar root directory : 

````
sh tools/build-GCstar-mingw64.sh msys2_packages
sh tools/build-GCstar-mingw64.sh perl_dependencies
sh tools/build-GCstar-mingw64.sh optional_dependencies
sh tools/build-GCstar-mingw64.sh custom_libgd
sh tools/build-GCstar-mingw64.sh optional_GD_dependencies
````

## Updating after installation ##

Once a GCstar version is installed, the plugins and some other files can be updated from the current commit from this repository.

On Windows, use the "Update plugins" entry in the GCstar launch group. Depending on our installation, it may require elevated privileges (run as admin). On other systems, run "gcstar -u" or "sudo gcstar -u" from the commande line.

GCstar can also be manually updated from this repository

- Download the [latest source code](https://gitlab.com/GCstar/GCstar/-/archive/main/GCstar-main.zip for the Gtk3 version) from Gitlab
- Unzip the archive in a temporary folder
- Copy the sub-folder **gcstar/lib/gcstar** under this folder to the sub-folder **lib** under the folder where you installed GCstar (for example "c:\Program Files\GCstar\lib" on Windows)
- Run GCstar to benefit from the updates and latest plugins

## Dependencies

On all platforms, to run a minimal GCstar, the following *Perl* modules are required

````
Gtk3
Gtk3::SimpleList
XML::Simple
````

To get a fully functional version of GCstar (statistics, importation/exportation, archives), the following Perl modules are required

````
Archive::Tar
Archive::Zip
Compress::Zlib
Date::Calc
DateTime::Format::Strptime
Digest::MD5
GD
GD::Graph
GD::Text
Image::ExifTool
MIME::Base64
MP3::Info
MP3::Tag
Net::FreeDB
Ogg::Vorbis::Header::PurePerl
Time::Piece
````

On some operating systems, some Perl librairies may not be packaged and may require a manual CPAN installation, for example:

````
cpan install XML::Simple
cpan install DateTime::Format::Strptime
cpan install JSON
````




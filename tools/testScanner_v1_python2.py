#!python
# -*- coding: utf-8 -*-
##############################################################
#
# emulation of GCstar scanner
#
# args:
#      port to use
#      ISBN code or name of file containing a list of codes
#

import sys
import time
import socket

codes = []

nbCodes = 0;

looping = False
if len(sys.argv) > 2:
    if sys.argv[2] == '--loop':
        looping = True
        file = 3
    else:
        looping = False
        file = 2
    print looping, file
    try:
        with open(sys.argv[file]) as f:
            for l in f.readlines():
                codes.append(l.strip())
    except:
        codes.append(sys.argv[file])
else:
    codes.append('9782723493406')
    codes.append('9782330024307')

print codes

print "Starting"
s = socket.socket(
    socket.AF_INET, socket.SOCK_STREAM)

s.connect(('127.0.0.1', int(sys.argv[1])))
print "Connected"

# 9782330024307 : Silo
# 9782266206907 : Secrets (Robert Jordan) => NooSFere
# 2265081574 : Le réveil des Titans => NooSFere
# 2723493407 : Vertical 2
# 2330024304 : Silo => Amazon
# 9782723493406



def sendEAN(code):
    global nbCodes

    nbCodes += 1
    data = "<scans><scan format='EAN_13'>"+code+"</scan><location>unEndroit</location><tags>UnTag</tags></scans>\n"
    print "Sending ", nbCodes, " : ", data
    s.send(data)
    print "Sent : "+data
    sys.stdout.flush()
    result = s.recv(4096)
    print "Result : "+result
    sys.stdout.flush()
    time.sleep(6)
    print "Looping"
    sys.stdout.flush()
    #i = raw_input().strip()
    i = 'continue'
    if i == 'end': sys.exit()

loop = True
while loop:
    print "Sending all codes"
    for code in codes:
        sendEAN(code)
    loop = looping

    
    


# GCstar (EN)
Desktop application to manage of various types of collections (books, comics, films, TV shows, music, games, wines, stamp, coins, etc...)

(version française ci-dessous)

## Software

The application is written in Perl with Gtk modules : it runs on Linux, Windows and MacOS. The latest version is 1.8.0. 

GCstar can be used with 4 companion apps:

* [GCstar Viewer](https://f-droid.org/fr/packages/com.gcstar.viewer/), an Android application to browse collections
* [GCstar Scan NG](https://gitlab.com/GCstar/GCstar_Scan_NG) and [GCstar Scanner](https://f-droid.org/fr/packages/com.gcstar.scanner/) (older version), two Android applications to add items to a collection by scanning their barcode
* [GCweb](https://gitlab.com/snouf/gcweb), a web app to publish GCstar collections on the Internet.

## Project

The [GCstar](http://www.gcstar.org/) was initiated by Tian and the source code was available under GPL licence on the [SVN repository](http://gna.org/projects/gcstar) (closed in May 2017). 

GCstar uses a plugin system to gather information on collection items by scraping various web sites. The development of these plugins is currently very distributed : some appear in forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ), others in open source repositories. Plugins can be updated without reinstalling GCstar.

This repository was created directly from the SVN repo using **git-svn**, trying to preserve tags and authors (some emails must be wrong though). The original [master](https://gitlab.com/GCstar/GCstar/tree/master) branch was pulled directly from upstream "trunk" (latest synchronisation **2017/05/15**). The [main](https://gitlab.com/GCstar/GCstar/tree/main) branch is now the Gtk3 version of GCstar as Gtk2 is deprecating. The [Gtk2](https://gitlab.com/GCstar/GCstar/tree/Gtk2) branch should be used for environments limited to Gtk2, most evolutions to GCstar are still backported in this branch. As the original SVN repository is now unavailable, this GIT repository is now the best way to get the source code of GCstar and its latest developments.

## Documentation

As the original documentation is now unavailable, a [new version](https://gcstar.gitlab.io/gcstar_docs) was initiated in the [GCstar_Docs](https://gitlab.com/GCstar/gcstar_docs) Gitlab repository but it's still in an early and incomplete stage. Up to recently, there was also a [discussion forum](http://forums.gcstar.org/) but it closed in 2022 for security reasons. Support may now be found in a dedicated [Discord server](https://discord.gg/W44WD8B2GR) or by creating [an issue](https://gitlab.com/GCstar/GCstar/-/issues/new) on Gitlab.
 
## Installation

Depending on your environment, [installing GCstar](resources/Installation.md) may be easy or may require some manual steps.

## Plugins

GCstar uses a plugin architecture to get information on items of collections from web sites. These plugins can easily be modified to adapt to changes of the web sites and can be updated without reinstalling GCstar (**gcstar -u**).

A [list of plugins and their status](resources/Plugins.md) is used to keep track of changes.

## Contributions

Software contributions are welcome on this Gitlab repository by creating merge requests or in the [discussion server](https://discord.gg/W44WD8B2GR/) by proposing source code. Some help is also needed to [improve the documentation](https://gitlab.com/GCstar/gcstar_docs) or to [check or contribute translations](resources/Translations.md) for already supported languages or new languages.

# GCstar (FR)

Application de gestion de divers types de collections (livres, comics/bandes dessinées, films, séries T, musiques, jeux, timbres, pièces, vins...)

## Logiciel

GCstar est écrit en Perl avec des extension Gtk : il tourne sur Linux, Windows et MacOS. La dernière version est la 1.8.0.

GCstar peut être utilisé avec quatre applications complémentaires :

* [GCstar Viewer](https://f-droid.org/fr/packages/com.gcstar.viewer/), une application Android pour visualiser une collection
* [GCstar Scan NG](https://gitlab.com/GCstar/GCstar_Scan_NG) et [GCstar Scanner](https://f-droid.org/fr/packages/com.gcstar.scanner/) (version plus ancienne), deux applications Android pour entrer des éléments dans une collection à partir de leur code barre
* [GCweb](https://gitlab.com/snouf/gcweb), une application web pour publier ses collections GCstar sur Internet.

## Projet

Le projet d'origine créé par Tian est [GCstar](http://www.gcstar.org/). Le code source en license GPL était disponible sur un [dépôt SVN](http://gna.org/projects/gcstar) (fermé en mai 2017). 

GCstar utilise un système de plugins pour collecter de l'information sur des sites web. Les développements de ces plugins sont actuellement très répartis et accessibles via des forums ( https://forum.ubuntu-fr.org/viewtopic.php?id=1919911 , http://forums.gcstar.org/ ) ou sur des forges.

Ce dépôt a été créé à partir de la source SVN en utilisant **git-svn**, en essayant de préserver les tags et les auteurs (sans doute des erreurs dans les adresses mail). La branche d'origine [master](https://gitlab.com/GCstar/GCstar/tree/master) était destinée à rester synchronisée avec le "trunk" de SVN (dernière synchro **2017/05/15**). La branche principale [main](https://gitlab.com/GCstar/GCstar/tree/main) contient maintenant la version Gtk3 étant donnée l'obsolescence en cours de Gtk2. La branche [Gtk2](https://gitlab.com/GCstar/GCstar/tree/Gtk2) est la version à utiliser pour les environnements ne disposant que de Gtk2, la plupart des évolutions de GCstar y sont toujours intégrées.  Le dépôt SVN n'étant plus accessible (serveur fermé), ce dépôt GIT est désormais la référence pour le code source de GCstar. 

## Documentation

Comme la documentation d'origine est actuellement indisponible, une [nouvelle version](https://gcstar.gitlab.io/gcstar_docs) a été initialisée dans le dépôt Gitlab [GCstar_Docs](https://gitlab.com/GCstar/gcstar_docs) mais elle est encore très incomplète. Jusqu'en 2022, il existait un [forum de discussion](http://forums.gcstar.org/) mais il a été fermé pour des raisons de sécurité. De l'aide sur GCstar peut être obtenue sur un [serveur Discord dédié](https://discord.gg/W44WD8B2GR) ou en crééant une [issue](https://gitlab.com/GCstar/GCstar/-/issues/new) sur Gitlab.

## Installation

En fonction de votre environnement, [installer GCstar](resources/Installation.md) pourra être très facile ou nécessiter quelques opérations manuelles.

## Plugins

GCstar est basé sur une architecture à base de modules complémentaires (plugins) permettant d'enrichir les informations à partir de sites web. Ces modules peuvent facilement être modifiés pour prendre en compte les évolutions des sites et peuvent être mis à jour sans réinstaller GCstar (**gcstar -u**)

Une [liste des plugins](resources/Plugins.md) est utilisée pour tracer leur disponibilité et leurs évolutions.

## Contributions

Les contributions au code de GCstar sont les bienvenues sur ce dépôt Gitlab sous forme de "merge requests" ou sur le [serveur de discussion](https://discord.gg/W44WD8B2GR) sous forme de code source. De l'aide est aussi nécessaire pour compléter la [documentation](https://gitlab.com/GCstar/gcstar_docs) ou pour [vérifier ou compléter les traductions](resources/Translations.md) pour les langues déjà supportées ou pour de nouvelles traductions.





